import { createStore, combineReducers } from 'redux'
import TodoReducer from './TodosReducer'
import CompletesReducer from './CompletesReducer'

const reducers = combineReducers({
    todos: TodoReducer,
    completes: CompletesReducer
})
const store = createStore(reducers)
export default store